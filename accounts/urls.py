from django.urls import path
from django.contrib.auth.views import LoginView, LogoutView
from accounts.views import signup
from django.contrib.auth import views as auth_views

# from accounts import views as user_views
from accounts.views import ProfileCreateView


urlpatterns = [
    path("login/", LoginView.as_view(), name="login"),
    path(
        "logout/",
        LogoutView.as_view(template_name="registration/logged_out.html"),
        name="logout",
    ),
    path("signup/", signup, name="signup"),
    path("profile/", ProfileCreateView.as_view(), name="profile"),
    path(
        "password_reset/",
        auth_views.PasswordResetView.as_view(
            template_name="password_reset/password_reset_form.html"
        ),
        name="password_reset",
    ),
    path(
        "password/change/done/",
        auth_views.PasswordChangeDoneView.as_view(
            template_name="password_change_done.html"
        ),
        name="password_change_done",
    ),
    # --------
    path(
        "password_change/",
        auth_views.PasswordChangeView.as_view(
            template_name="password_reset/password_change.html"
        ),
        name="password_change",
    ),
    path(
        "password_reset/done/",
        auth_views.PasswordResetCompleteView.as_view(
            template_name="password_reset/password_reset_done.html"
        ),
        name="password_reset_done",
    ),
    path(
        "reset/<uidb64>/<token>/",
        auth_views.PasswordResetConfirmView.as_view(),
        name="password_reset_confirm",
    ),
    path(
        "reset/done/",
        auth_views.PasswordResetCompleteView.as_view(
            template_name="password_reset/password_reset_complete.html"
        ),
        name="password_reset_complete",
    ),
]
