# from pyexpat.errors import messages
from django.contrib.auth import login
from django.contrib.auth.models import User

# from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect
from .forms import UserRegisterForm
from accounts.models import Profile
from django.urls import reverse_lazy
from django.views.generic.edit import CreateView
from django.contrib.auth.mixins import LoginRequiredMixin


def signup(request):
    if request.method == "POST":
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password1")
            user = User.objects.create_user(
                username=username,
                password=password,
            )
            user.save()
            login(request, user)
            return redirect("home")
    else:
        form = UserRegisterForm()
    context = {
        "form": form,
    }
    return render(request, "registration/signup.html", context)


# messages.success(
#    request, f"Your account has been created! Please login."


class ProfileCreateView(LoginRequiredMixin, CreateView):
    model = Profile
    template_name = "User/profile.html"
    fields = ["user", "bio", "goals", "image"]

    def get_success_url(self):
        return reverse_lazy("show_profile", args=[self.object.id])


# def profile(request):
# return render(request, 'profile.html')
