# from pyexpat import model
# from re import template
# from django.shortcuts import render

from projects.models import Project
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy


# Create your views here.


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    template_name = "create.html"
    fields = ["name", "description", "members"]

    # need to redirect to detail page...
    def get_success_url(self):
        return reverse_lazy("show_project", args=[self.object.id])


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "list.html"

    def get_queryset(self):
        return Project.objects.filter(members=self.request.user)


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "detail.html"
    context_object_name = "project"
