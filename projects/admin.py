from django.contrib import admin
from projects.models import Project

# Register your models here.

# Doing this allows us to see the project model
# in the admin site


class ProjectAdmin(admin.ModelAdmin):
    pass


admin.site.register(Project, ProjectAdmin)
