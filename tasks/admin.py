from django.contrib import admin
from tasks.models import Task
from accounts.models import Profile


class TaskAdmin(admin.ModelAdmin):
    pass


admin.site.register(Task, TaskAdmin)
admin.site.register(Profile)
